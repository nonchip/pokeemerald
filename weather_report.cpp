#include <iostream>
#include <chrono>
#include <string>
#define NCHI__HOUR_DAY_START 8
#define NCHI__HOUR_NIGHT_START 20

enum
{
    MONTH_JAN = 1,
    MONTH_FEB,
    MONTH_MAR,
    MONTH_APR,
    MONTH_MAY,
    MONTH_JUN,
    MONTH_JUL,
    MONTH_AUG,
    MONTH_SEP,
    MONTH_OCT,
    MONTH_NOV,
    MONTH_DEC
};
static const int32_t sNumDaysInMonths[12] =
{
    31,
    28,
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31,
};
bool IsLeapYear(uint32_t year)
{
    if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
        return true;

    return false;
}

uint16_t ConvertDateToDayCount(uint8_t year, uint8_t month, uint8_t day)
{
    int32_t i;
    uint16_t dayCount = 0;

    for (i = year - 1; i >= 0; i--)
    {
        dayCount += 365;

        if (IsLeapYear(i) == true)
            dayCount++;
    }

    for (i = 0; i < month - 1; i++)
        dayCount += sNumDaysInMonths[i];

    if (month > MONTH_FEB && IsLeapYear(year) == true)
        dayCount++;

    dayCount += day;

    return dayCount;
}

char weather(uint16_t idnum1, uint8_t idnum2, uint8_t idnum3, uint16_t days, uint8_t month, uint8_t hours){

    if ((days + hours/4 + idnum1) % 420 == 0)
        return '!';

    if (hours >= NCHI__HOUR_NIGHT_START || hours < NCHI__HOUR_DAY_START) return '-';

    if ((days + hours/2 + idnum2) % 5 == 0){
        if (month >= MONTH_APR && month <= MONTH_JUN) return 'r';
        if (month >= MONTH_JUN && month <= MONTH_AUG) return ' ';
        if (month >= MONTH_SEP && month <= MONTH_OCT) return 't';
        if (month <= MONTH_MAR || month >= MONTH_NOV) return '*';
    }
    if ((days + hours/2 + idnum3) % 3 == 0){
        if (month >= MONTH_APR && month <= MONTH_AUG) return 'c';
        if (month >= MONTH_SEP && month <= MONTH_OCT) return 'r';
        if (month <= MONTH_MAR || month >= MONTH_NOV) return '-';
    }
    return ' ';
}


int main(int argc, char* argv[]){

    if(argc < 1) return 1;

    auto str = std::string(argv[1]);
    if(str.size()!=8) return 1;
    auto bytes = std::stoul(str,nullptr,16);

    const uint8_t b0 = (bytes>> 0)&0xff;
    const uint8_t b1 = (bytes>> 8)&0xff;
    const uint8_t b2 = (bytes>>16)&0xff;
    const uint8_t b3 = (bytes>>24)&0xff;

    const uint16_t idnum1 = b0 + b2;
    const uint8_t idnum2 = b1;
    const uint8_t idnum3 = b3;

    auto now = std::chrono::system_clock::now();
    auto tt = std::chrono::system_clock::to_time_t(now);
    auto tm = *localtime(&tt);

    std::cout << '\t';
    for(int hour = 0 ; hour <= 23 ; hour ++)
        std::cout << hour << '\t';
    std::cout << std::endl;

    for(int days = 0; days <= 15 ; days ++){
        tt = std::chrono::system_clock::to_time_t(now);
        tm = *localtime(&tt);
        const uint16_t day = ConvertDateToDayCount(tm.tm_year-100,tm.tm_mon+1,tm.tm_mday);
        std::cout << day << '\t';
        for(int hour = 0 ; hour <= 23 ; hour ++){
            std::cout << weather(idnum1,idnum2,idnum3,day,tm.tm_mon+1,hour) << '\t';
        }
        std::cout << std::endl;
        now += std::chrono::hours(24);
    }
    return 0;
}

