#pragma once

#define NCH__BUGFIX 2
// 1: enables various bugfixes
// 2: also clean up bullshit the gamefreak intern mistook for code

#define NCH__ADD_VERSION_EXCLUSIVES 1
// 1: add R/S version exclusive pokemon in their respective spots
//    except surskit, because that's EVERYWHERE. added that to the TV outbreaks though

#define NCH__SAVE_DISABLE_CONFIRMATION 1
// 1: disable "Do you want to overwrite an existing save?" dialog.
//    turns out people tend to like saving their game more than once.

#define NCH__SAVE_COMPLETED_TIMER 4 /* seconds */
// >0: set the timer on "You saved the game." message, might be helpful on slow flash cartridges
#define NCH__SAVE_COMPLETED_TIMER_ENFORCE 1
// 1: disable closing that message early by pressing A

#define NCH__BERRYTREE_LESS_CHATTY 1
// 1: makes berry tree interactions less convoluted

#define NCH__MEVENTEREADER_REENABLED 1
// 1: enable the Japanese-only "Mystery Event" and "Mystery Gift over eReader" features
//    (they unlock with the normal "Mystery Gift" by putting "LINK TOGETHER WITH ALL" in the PokeMart questionnaire)

#define NCH__RTC_RESET_MENU_ENABLED 1
// 1: enable the RTC reset menu (Select + Left + B on title screen)
//    usually nintendo support would do this via link cable

#define NCH__ENDGAME_CHAMPION_INFO_ITEM 1
// 1: Gives you a "Champion's Info" item after entering HOF the first time
//    - this tells you your complete Trainer ID (the "secret ID" is the upper half)
//    - if ENDGAME_SHINYMULT is on this will also show the multiplier
//    - if ENDGAME_MOREROAMERS is on this will also show the roamer's species

#define NCH__ENDGAME_SHINYMULT 1 /* multiplied with Elite4 wins = total chances */
// >0: gives you additional chances at any encountered pokemon being shiny for beating Elite 4 repeatedly

#define NCH__ENDGAME_MOREROAMERS 3
// 1: endless roamers after Latios/Latias (initial roamer unchanged):
//    - instead of disabling the roaming pokemon it becomes a random species (prefers uncaught species, with a higher weight depending on how often you beat Elite 4)
//    - there's an audiovisual cue when you're nearby
//    - they may over time get hurt on their travels (if it faints due to this, it gets respawned*)
//    - if you catch it or it faints, it becomes replaced by another random species
// 2: like 1, but prevents event species (unless caught already)
// 3: like 2, but prevents unseen species (use moreoutbreaks for those), (*)and it gets replaced by a random if it faints on its travels

#define NCH__ENDGAME_MOREOUTBREAKS 4
// 1: add randomized outbreaks
//     beating Elite4 more often increases the chance for them to be unseen pokemon
// 2: like 1, but prevents event species
// 3: like 2, but makes outbreaks more common when beating elite 4 more often
// 4: like 3, but tries to start one outbreak per week

#define NCH__ENDGAME_SHOWPID 2
// 1: shows you a mon's personality ID in its summary screen (in the contest move description when no move is selected)
// 2: also shows IVs and EVs (HP, ATK, DEF, SPEED, SPATK, SPDEF)

#define NCH__ENDGAME_EXCHANGECORNER_EVENTS 1
// 1: allows to buy event key items in the exchange corner
//    PLEASE do not look up how i did that, nintendo's code was bad enough to begin with!

#define NCH__ENDGAME_LEVEL_SCALED_TRAINERS 5
// >0: trainer battles after beating HOF are level scaled (not in BF though)
//     number is the max random deviation from your calculated average level
//     (the average is weighted in favour of your high level mons by the number*2)

#define NCH__ENDGAME_SELL_DEEPSEA 3
// 1: Secret/Hidden Power seller in Slateport Market sells DeepSeaTooth/Scale in endgame after choosing one
// 2: also increase the price because 200 is too low
// 3: also make it a one-time offer

#define NCH__DAYNIGHTCYCLE 1
// 1: "shade" weather at night, random snow in winter, and a few other weather effects

#define NCH__EXPSHARE_IS_LUCKYEGG 0
// 1: EXP SHARE counts as LUCKY EGG for exp multipliers
#define NCH__GEN6_EXPSHARE 1
// 1: EXP SHARE works for the whole party (as in Gen6)

#define NCH__GEN5_TMS 2
// 1: TMs are infinite (as in Gen5)
// 2: Secret/Hidden Power seller in Slateport Market sells "obtained but spent" (in earlier versions) TMs in endgame

#define NCH__GEN4_POKECENTER_EGGS 1
// 1: Pokecenters ignore eggs (as in Gen4)

#define NCH__GEN7_PREMIERBALLS 1
// 1: gives premier balls per 10 balls, instead of one for >9 (as in Gen7)

#define NCH__GEN4_MOVES_PSS 1
// 1: enable "Physical/Special split" (as in Gen4)

#define NCH__GEN6_FAIRY 0
// SOOOOO BROKEN!
// 1: enable fairy type (as in Gen6)
// 2: also update other type effectiveness to Gen6

#define NCH__GEN2_SHUCKLE_JUICE 1
// 1: shuckle can randomly turn held berries into Berry Juice (as in Gen2)

#define NCH__BETTER_BALLS 1
// 1: some underappreciated balls improved:
//    - Luxury ball has catchrate of Great ball
//    - Premier ball has catchrate of Ultra ball

#define NCH__INCURABLE_POKERUS_STRAIN 1
// 1: rare (1/16 chance when contracting) incurable pokerus strain (will spread as a normal 4-day strain)

#define NCH__PERSONALITY_INHERITANCE 1
// 1: enables chance of inheriting parent personality when breeding:
//    - first 50% chance of inheriting upper half (XXXX0000)
//    - then 50% chance of inheriting lower half (0000xxxx)
//      (which parent gives which half is random, but never both from the same)
//    - finally 50% chance to shift the result by a quarter (XXXXxxxx -> xxXXXXxx)

#define NCH__IV_INHERITANCE 1
// 1: small additional chance of inheriting more IVs

#define NCH__RUN_INDOORS 1
// 1: allows running indoors

#define NCH__STOP_LOW_HEALTH_BLEEP 1
// 1: stops the "low health beep" after the next action

#define NCH__KEYBOARD_AUTO_LOWER_CASE 1
// 1: makes the keyboard switch to lower case after the first letter

#define NCH__PICKUP_REPLACE 3
// 1: replace hardcoded "unique" items (TMs and hold items) in "rare pickup" roll with randomized items (excluding "important" items)
// 2: also replace other items randomly
// 3: and increase chance with level

#define NCH__BERRIES_WEATHER 2
// 1: berries get influenced by weather (more rain = better, volcanic ash = better, desert = worse)
// 2: also winter = slightly bad

#define NCH__BERRIES_DONT_DIE 2
// 1: prevents berries from vanishing after a certain time
// 2: also makes them stay fully grown instead of regrowing

#define NCH__FIX_EVO_TIMING 1
// 1: makes "day"/"night" for evolution actually day and night (instead of morning and evening)

#define NCH__2IN1_BIKE 1
// 1: allows swapping bikes on the fly by pressing R

#define NCH__RECALC_STATS_ON_EV_GAIN 2
// 1: recalculate stats when a mon gains EVs (makes "Box Trick" unnecessary)
// 2: also allows EV gain >lv100

#define NCH__FLUTES_ARE_UNIQUE 1
// 1: makes the Flutes unique items, to prevent buying duplicates

#define NCH__SOUND_IPATIX 1
// 1: use ipatix' sound driver instead of nintendo's m4a

#define NCH__MIRAGEISLAND_FROM_PC 1
// 1: factors all PC mons into the mirage island check

#define NCH__LINK_STREAM 0
// 1: streams game data via link port (unless link port is actively used by the game) (BROKEN)

#define NCH__SHELLBELL_PRICE 12000
// >0: how much a SHELL BELL is worth (because it can be farmed daily but sells for nothing which is dumb)

#define NCH__PLAYTIME_OVERFLOWS 1
// 1: overflow the play time to 0 after 999:59:59