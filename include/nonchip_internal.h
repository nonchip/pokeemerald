#pragma once
#include "nonchip_config.h"

#if NCH__GEN6_EXPSHARE && NCH__EXPSHARE_IS_LUCKYEGG
#error "Cannot have both NCH__EXPSHARE_IS_LUCKYEGG and NCH__GEN6_EXPSHARE enabled!"
#endif

#define NCHI__MOVE_I2EWRAM (NCH__SOUND_IPATIX)

#define NCHI__HOUR_DAY_START 8
#define NCHI__HOUR_NIGHT_START 20

#define NCHI__CLEANUP_GF_BS (NCH__ENDGAME_LEVEL_SCALED_TRAINERS || (NCH__BUGFIX>1) || NCH__SOUND_IPATIX || (NCH__ENDGAME_MOREOUTBREAKS>= 3))

#define NCHI__SCROLL_MULTI_CUSTOM (NCH__ENDGAME_EXCHANGECORNER_EVENTS)
/* USAGE:
  setvar VAR_0x8004, SCROLL_MULTI_NCH_CUSTOM
  setvar VAR_0x800B, 3 @ Number of options
  bufferstring STR_VAR_2, my_options
  special ShowScrollableMultichoice
  waitstate
  switch VAR_RESULT
  case 0, option_0
  case 1, option_1
  case 2, option_2
  case MULTI_B_PRESSED, option_exit
  end

my_options:
  .string "Option A\nOption B\nOption C$"

option_0:
option_1:
option_2:
option_exit:
  end
*/

#define NCHI__DECLARE_ConvertIntToHexStringN_unsigned (NCH__ENDGAME_CHAMPION_INFO_ITEM || NCH__ENDGAME_SHOWPID)
#define NCHI__DECLARE_CreateInitialRoamerMon (NCH__ENDGAME_MOREROAMERS)
#define NCHI__DECLARE_CountPartyNonEggMons (NCH__GEN4_POKECENTER_EGGS)

#define NCHI__FIELD_INPUT_LR (NCH__2IN1_BIKE)

#define NCHI__LINK_KEEP_NORMAL_OPEN (NCH__LINK_STREAM)

#define NCHI__ADDITIONAL_ITEMS_COUNT (0\
  + !!(NCH__ENDGAME_CHAMPION_INFO_ITEM)\
)

#define NCHI__DYNAMIC_SHOP ((NCH__GEN5_TMS >= 2) || NCH__ENDGAME_SELL_DEEPSEA)

#define NCHI__TRACK_HOF (NCH__ENDGAME_SHINYMULT||NCH__ENDGAME_MOREROAMERS)
// tracks HOF wins additionally in a 16bit number instead of the usual 999, for internal calculations.

#define NCHI__ADDITIONAL_SAVE_FLAGS (NCH__GEN6_EXPSHARE || (NCH__ENDGAME_SELL_DEEPSEA>=3))
#define NCHI__ADDITIONAL_SAVE_VARS (NCHI__TRACK_HOF || NCH__PLAYTIME_OVERFLOWS)
#define NCHI__ADDITIONAL_SAVE_PAYLOAD (NCHI__ADDITIONAL_SAVE_FLAGS||NCHI__ADDITIONAL_SAVE_VARS)
// adds a `u8 gnAdditionalSaveData[N_ADDITIONAL_SAVE_PAYLOAD_SIZE == 1624 == 0x658]` stored in the previously unused padding in each save slot
// flags/vars can be accessed via IDs 0xF000+n:
//   for vars (16bit), this will point to gnAdditionalSaveData[n*2]
//   for flags (1bit), to gnAdditionalSaveData[n/8]
// (so make sure not to overlap them and leave space for flags at the start of your data!)
// my convention:
//   bool[0x2d2] flags    at IDs 0xF000..0xF2D1 ; bytes 0x000..0x05A
//   u16 [0x2d2] vars     at IDs 0xF02E..0xF2FF ; bytes 0x05C..0x5FF
//   u8  [0x058] metadata at                      bytes 0x600..0x658
// TODO: maybe figure out a way of "importing" an old save so we can get rid of a lot of redundant data
//       (see: https://github.com/LOuroboros/pokeemerald/commit/515c7cd3a345611f1fe755c7f71b1ba1e20bb88b)
//       (also maybe: https://www.pokecommunity.com/showthread.php?p=10168472#post10168472)

#define NCHI__SPECIES_IS_SPECIAL(species) ( \
/* https://bulbapedia.bulbagarden.net/wiki/List_of_in-game_event_Pok%C3%A9mon_(Emerald) */ \
  /* egg in lavaridge   */ (species >= SPECIES_WYNAUT && species <= SPECIES_WOBBUFFET) || \
  /* weather institute  */ species == SPECIES_CASTFORM || \
  /* devon scope        */ species == SPECIES_KECLEON || \
  /* fossil             */ (species >= SPECIES_LILEEP && species <= SPECIES_ARMALDO) || \
  /* steven in mossdeep */ (species >= SPECIES_BELDUM && species <= SPECIES_METAGROSS) || \
  /* story legendaries  */ species == SPECIES_GROUDON || species == SPECIES_KYOGRE || species == SPECIES_RAYQUAZA || \
  /* regis              */ species == SPECIES_REGISTEEL || species == SPECIES_REGIROCK || species == SPECIES_REGIROCK || \
  /* lati*s             */ species == SPECIES_LATIOS || species == SPECIES_LATIAS || \
                                                                                          /* Only one Johto starter is obtainable, so allow the others when it is. */ \
  /* johto starters     */ ((species >= SPECIES_CHIKORITA && species <= SPECIES_CROCONAW) && !HasAllHoennMons()) || \
  /* BF cut             */ species == SPECIES_SUDOWOODO || \
                                                  /* MEWTWO is unobtainable, so allow it after getting MEW. */ \
  /* old sea map        */ species == SPECIES_MEW || (species == SPECIES_MEWTWO && GetSetPokedexFlag(SpeciesToNationalPokedexNum(SPECIES_MEW), FLAG_GET_CAUGHT) == 0) || \
  /* mystic ticket      */ species == SPECIES_LUGIA || species == SPECIES_HO_OH || \
  /* aurora ticket      */ species == SPECIES_DEOXYS || \
  /* altering cave 2    */ (species >= SPECIES_MAREEP && species <= SPECIES_AMPHAROS) || \
  /* altering cave 3    */ (species >= SPECIES_PINECO && species <= SPECIES_FORRETRESS) || \
  /* altering cave 4    */ (species >= SPECIES_HOUNDOUR && species <= SPECIES_HOUNDOOM) || \
  /* altering cave 5    */ (species >= SPECIES_TEDDIURSA && species <= SPECIES_URSARING) || \
  /* altering cave 6    */ species == SPECIES_AIPOM || \
  /* altering cave 7    */ species == SPECIES_SHUCKLE || \
  /* altering cave 8    */ species == SPECIES_STANTLER || \
  /* altering cave 9    */ species == SPECIES_SMEARGLE || \
  /* UNOBTAINABLE EVENT */ ((species == SPECIES_JIRACHI || species == SPECIES_CELEBI) && !HasAllHoennMons()) \
)
