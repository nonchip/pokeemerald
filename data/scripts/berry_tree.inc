BerryTreeScript::
	special ObjectEventInteractionGetBerryTreeData
	switch VAR_0x8004
	case BERRY_STAGE_SPARKLING, BerryTree_EventScript_Sparkling
	case BERRY_STAGE_NO_BERRY, BerryTree_EventScript_CheckSoil
	case BERRY_STAGE_PLANTED, BerryTree_EventScript_CheckBerryStage1
	case BERRY_STAGE_SPROUTED, BerryTree_EventScript_CheckBerryStage2
	case BERRY_STAGE_TALLER, BerryTree_EventScript_CheckBerryStage3
	case BERRY_STAGE_FLOWERING, BerryTree_EventScript_CheckBerryStage4
	case BERRY_STAGE_BERRIES, BerryTree_EventScript_CheckBerryFullyGrown
	end

BerryTree_EventScript_Sparkling::
	lockall
	message BerryTree_Text_ExclamationPoint
	waitmessage
	waitbuttonpress
	releaseall
	end

BerryTree_EventScript_CheckSoil::
	lock
	faceplayer
	specialvar VAR_RESULT, PlayerHasBerries
	.if NCH__BERRYTREE_LESS_CHATTY
		goto_if_eq VAR_RESULT, TRUE, BerryTree_EventScript_ChooseBerryToPlant
	.else
		goto_if_eq VAR_RESULT, TRUE, BerryTree_EventScript_WantToPlant
	.endif
	message BerryTree_Text_ItsSoftLoamySoil
	waitmessage
	waitbuttonpress
	release
	end

BerryTree_EventScript_WantToPlant::
	msgbox BerryTree_Text_WantToPlant, MSGBOX_YESNO
	goto_if_eq VAR_RESULT, YES, BerryTree_EventScript_ChooseBerryToPlant
	goto_if_eq VAR_RESULT, NO, BerryTree_EventScript_CancelPlanting
	end

BerryTree_EventScript_ChooseBerryToPlant::
	fadescreen FADE_TO_BLACK
	closemessage
	special Bag_ChooseBerry
	waitstate
	goto_if_eq VAR_ITEM_ID, 0, BerryTree_EventScript_CancelPlanting
	removeitem VAR_ITEM_ID
	call BerryTree_EventScript_PlantBerry

BerryTree_EventScript_CancelPlanting::
	release
	end

BerryTree_EventScript_CheckBerryStage1::
	lockall
	special ObjectEventInteractionGetBerryCountString
	.if NCH__BERRYTREE_LESS_CHATTY
		checkitem ITEM_WAILMER_PAIL, 1
		compare VAR_RESULT, 0
		goto_if_eq BerryTree_EventScript_DontWater
		msgbox BerryTree_Text_BerryGrowthStage1, MSGBOX_YESNO
		compare VAR_RESULT, YES
		goto_if_eq BerryTree_EventScript_WaterBerry
		compare VAR_RESULT, NO
		goto_if_eq BerryTree_EventScript_DontWater
	.else
		message BerryTree_Text_BerryGrowthStage1
		waitmessage
		waitbuttonpress
		goto BerryTree_EventScript_WantToWater
	.endif

BerryTree_EventScript_CheckBerryStage2::
	lockall
	special ObjectEventInteractionGetBerryName
	.if NCH__BERRYTREE_LESS_CHATTY
		checkitem ITEM_WAILMER_PAIL, 1
		compare VAR_RESULT, 0
		goto_if_eq BerryTree_EventScript_DontWater
		msgbox BerryTree_Text_BerryGrowthStage2, MSGBOX_YESNO
		compare VAR_RESULT, YES
		goto_if_eq BerryTree_EventScript_WaterBerry
		compare VAR_RESULT, NO
		goto_if_eq BerryTree_EventScript_DontWater
	.else
		message BerryTree_Text_BerryGrowthStage2
		waitmessage
		waitbuttonpress
		goto BerryTree_EventScript_WantToWater
	.endif

BerryTree_EventScript_CheckBerryStage3::
	lockall
	special ObjectEventInteractionGetBerryName
	.if NCH__BERRYTREE_LESS_CHATTY
		checkitem ITEM_WAILMER_PAIL, 1
		compare VAR_RESULT, 0
		goto_if_eq BerryTree_EventScript_DontWater
		msgbox BerryTree_Text_BerryGrowthStage3, MSGBOX_YESNO
		compare VAR_RESULT, YES
		goto_if_eq BerryTree_EventScript_WaterBerry
		compare VAR_RESULT, NO
		goto_if_eq BerryTree_EventScript_DontWater
	.else
		message BerryTree_Text_BerryGrowthStage3
		waitmessage
		waitbuttonpress
		goto BerryTree_EventScript_WantToWater
	.endif

BerryTree_EventScript_CheckBerryStage4::
	call BerryTree_EventScript_GetCareAdverb
	lockall
	special ObjectEventInteractionGetBerryName
	.if NCH__BERRYTREE_LESS_CHATTY
		checkitem ITEM_WAILMER_PAIL, 1
		compare VAR_RESULT, 0
		goto_if_eq BerryTree_EventScript_DontWater
		msgbox BerryTree_Text_BerryGrowthStage4, MSGBOX_YESNO
		compare VAR_RESULT, YES
		goto_if_eq BerryTree_EventScript_WaterBerry
		compare VAR_RESULT, NO
		goto_if_eq BerryTree_EventScript_DontWater
	.else
		message BerryTree_Text_BerryGrowthStage4
		waitmessage
		waitbuttonpress
		goto BerryTree_EventScript_WantToWater
	.endif

@ VAR_0x8005 here is the number of times watered
@ Buffered by ObjectEventInteractionGetBerryTreeData
BerryTree_EventScript_GetCareAdverb::
	goto_if_eq VAR_0x8005, 0, BerryTree_EventScript_SetAdverbPoor
	goto_if_eq VAR_0x8005, 4, BerryTree_EventScript_SetAdverbGreat
	bufferstring STR_VAR_2, BerryTree_Text_CareAdverbGood
	return

BerryTree_EventScript_SetAdverbGreat::
	bufferstring STR_VAR_2, BerryTree_Text_CareAdverbGreat
	return

BerryTree_EventScript_SetAdverbPoor::
	bufferstring STR_VAR_2, BerryTree_Text_CareAdverbPoor
	return

@ VAR_0x8006 here is the number of berries
BerryTree_EventScript_CheckBerryFullyGrown::
	buffernumberstring STR_VAR_2, VAR_0x8006
	lock
	faceplayer
	.if NCH__BERRYTREE_LESS_CHATTY
		goto BerryTree_EventScript_PickBerry
	.else
		special ObjectEventInteractionGetBerryCountString
		msgbox BerryTree_Text_WantToPick, MSGBOX_YESNO
		goto_if_eq VAR_RESULT, YES, BerryTree_EventScript_PickBerry
		goto_if_eq VAR_RESULT, NO, BerryTree_EventScript_CancelPickingBerry
	.endif

BerryTree_EventScript_PickBerry::
	special ObjectEventInteractionPickBerryTree
	goto_if_eq VAR_0x8004, 0, BerryTree_EventScript_BerryPocketFull
	special IncrementDailyPickedBerries
	special ObjectEventInteractionRemoveBerryTree
	message BerryTree_Text_PickedTheBerry
	playfanfare MUS_OBTAIN_BERRY
	waitmessage
	waitfanfare
	.if !(NCH__BERRYTREE_LESS_CHATTY)
		waitbuttonpress
		message BerryTree_Text_PutAwayBerry
		waitmessage
		waitbuttonpress
	.endif
	release
	end

BerryTree_EventScript_BerryPocketFull::
	message BerryTree_Text_BerryPocketFull
	waitmessage
	waitbuttonpress
	release
	end

BerryTree_EventScript_CancelPickingBerry::
	message BerryTree_Text_BerryLeftUnpicked
	waitmessage
	waitbuttonpress
	release
	end

BerryTree_EventScript_ItemUsePlantBerry::
	lockall
	special ObjectEventInteractionGetBerryTreeData
	call BerryTree_EventScript_PlantBerry
	releaseall
	end

BerryTree_EventScript_WantToWater::
	checkitem ITEM_WAILMER_PAIL
	goto_if_eq VAR_RESULT, FALSE, BerryTree_EventScript_DontWater
	special ObjectEventInteractionGetBerryName
	msgbox BerryTree_Text_WantToWater, MSGBOX_YESNO
	goto_if_eq VAR_RESULT, YES, BerryTree_EventScript_WaterBerry
	goto_if_eq VAR_RESULT, NO, BerryTree_EventScript_DontWater

BerryTree_EventScript_DontWater::
	releaseall
	end

BerryTree_EventScript_ItemUseWailmerPail::
	special ObjectEventInteractionGetBerryTreeData
	lockall
BerryTree_EventScript_WaterBerry::
	.if !(NCH__BERRYTREE_LESS_CHATTY)
		special ObjectEventInteractionGetBerryName
		message BerryTree_Text_WateredTheBerry
		waitmessage
	.endif
	special ObjectEventInteractionWaterBerryTree
	special DoWateringBerryTreeAnim
	waitstate
	.if !(NCH__BERRYTREE_LESS_CHATTY)
		message BerryTree_Text_PlantIsDelighted
		waitmessage
		waitbuttonpress
	.endif
	releaseall
	end

BerryTree_EventScript_PlantBerry::
	special ObjectEventInteractionPlantBerryTree
	incrementgamestat GAME_STAT_PLANTED_BERRIES
	special IncrementDailyPlantedBerries
	.if !(NCH__BERRYTREE_LESS_CHATTY)
		special ObjectEventInteractionGetBerryCountString
		message BerryTree_Text_PlantedOneBerry
		waitmessage
		waitbuttonpress
	.endif
	return

BerryTree_Text_ItsSoftLoamySoil:
	.string "It's soft, loamy soil.$"

BerryTree_Text_WantToPlant:
	.string "It's soft, loamy soil.\n"
	.string "Want to plant a BERRY?$"

BerryTree_Text_PlantedOneBerry:
	.string "{PLAYER} planted one {STR_VAR_1} in\n"
	.string "the soft, loamy soil.$"

BerryTree_Text_BerryGrowthStage1:
	.if NCH__BERRYTREE_LESS_CHATTY
		.string "One {STR_VAR_1} was planted here.\n"
		.string "Want to water it with the WAILMER PAIL?$"
	.else
		.string "One {STR_VAR_1} was planted here.$"
	.endif

BerryTree_Text_BerryGrowthStage2:
	.if NCH__BERRYTREE_LESS_CHATTY
		.string "{STR_VAR_1} has sprouted.\n"
		.string "Want to water it with the WAILMER PAIL?$"
	.else
		.string "{STR_VAR_1} has sprouted.$"
	.endif

BerryTree_Text_BerryGrowthStage3:
	.if NCH__BERRYTREE_LESS_CHATTY
		.string "This {STR_VAR_1} plant is growing taller.\n"
		.string "Want to water it with the WAILMER PAIL?$"
	.else
		.string "This {STR_VAR_1} plant is growing taller.$"
	.endif

BerryTree_Text_BerryGrowthStage4:
	.string "These {STR_VAR_1} flowers are blooming\n"
	.if NCH__BERRYTREE_LESS_CHATTY
		.string "{STR_VAR_2}. Want to water them?$"
	.else
		.string "{STR_VAR_2}.$"
	.endif

BerryTree_Text_CareAdverbGreat:
	.string "very beautifully$"

BerryTree_Text_CareAdverbPoor:
	.string "cutely$"

BerryTree_Text_CareAdverbGood:
	.string "prettily$"

BerryTree_Text_WantToPick:
	.string "You found {STR_VAR_2} {STR_VAR_1}!\p"
	.string "Do you want to pick the\n"
	.string "{STR_VAR_1}?$"

BerryTree_Text_PickedTheBerry:
	.string "{PLAYER} picked the {STR_VAR_2} {STR_VAR_1}.$"

BerryTree_Text_PutAwayBerry:
	.string "{PLAYER} put away the {STR_VAR_1}\n"
	.string "in the BAG's BERRIES POCKET.\p"
	.string "The soil returned to its soft and\n"
	.string "loamy state.$"

BerryTree_Text_BerryPocketFull:
	.string "The BAG's BERRIES POCKET is full.\p"
	.string "The {STR_VAR_1} couldn't be taken.$"

BerryTree_Text_BerryLeftUnpicked:
	.string "{PLAYER} left the {STR_VAR_1}\n"
	.string "unpicked.$"

BerryTree_Text_WantToWater:
	.string "Want to water the {STR_VAR_1} with the\n"
	.string "WAILMER PAIL?$"

BerryTree_Text_WateredTheBerry:
	.string "{PLAYER} watered the {STR_VAR_1}.$"

BerryTree_Text_PlantIsDelighted:
	.string "The plant seems to be delighted.$"

BerryTree_Text_ExclamationPoint:
	.string "!$"
